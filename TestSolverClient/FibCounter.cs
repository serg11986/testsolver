﻿using System;

namespace TestCaseProblemSolver
{
    public static class FibCounter
    {
        public static long FibNumber(int n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("Номер числа Фиббоначи должен быть неотрицательным числом");

            if (n == 0)
                return 0;

            if (n == 1)
                return 1;

            long prev = 0, cur = 1;

            for (int i = 2; i <= n; i++)
            {
                long tmp = cur;
                checked
                {
                    cur += prev;
                }
                prev = tmp;
            }

            return cur;
        }
    }
}
