﻿using System;

namespace TestCaseProblemSolver
{
    public static class FactCounter
    {
        public static long FactOf(long n)
        {
            if (n < 0)
                throw new ArgumentOutOfRangeException("Факториал не определен для отрицательных чисел");

            if (n < 2)
                return 1;

            checked
            {
                return n * FactOf(n - 1);
            }
        }
    }
}
