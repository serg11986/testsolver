﻿namespace TestCaseProblemSolver
{
    public class MotorVesselSpeedCounter
    {
        EqSolver solver = new EqSolver();

        public double X { get; set; }
        public double D { get; set; }
        public double S { get; set; }

        public string Answer { get; private set; }
        public double V { get; private set; }

        public string Eq { get; private set; }


        public void Solve()
        {
            solver.A = X;
            solver.B = X * D;
            solver.C = -S * D;
            Eq = solver + "";
            solver.Solve();
            V = solver.X1.Value;
            Answer = "Скорость равна " + V + "км/ч";
        }
    }
}
