﻿using System;
using System.Text;

namespace TestCaseProblemSolver
{
    public class EqSolver
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }


        public double? X1 { get; set; }
        public double? X2 { get; set; }
        public string Answer { get; private set; }


        public void Solve()
        {

            if (A == 0 && B == 0)
            {
                if (C == 0)
                {
                    Answer = "Уравнение имеет бесконечное множество решений (верное равенство, не зависящее от переменной x - решением будет любое число)";
                }
                else
                {
                    Answer = "Уравнение не имеет решений (неверное равенство, не зависящее от переменной x)";
                }
                return;
            }


            if (A == 0)
            {
                X1 = X2 = -C / B;
                Answer = "Уравнение имеет один корень: x = " + X1;
                return;
            }


            double d = B* B -4 * A * C;

            if (d > 0)
            {
                X1 = (-B + Math.Sqrt(d)) / (2 * A);
                X2 = (-B - Math.Sqrt(d)) / (2 * A);
                Answer = "Уравнение имеет два корня: x1 = " + X1 + " , x2 = " + X2;
            }
            else if (d == 0)
            {
                X1 = X2 = -B / (2 * A);
                Answer = "Уравнение имеет один корень: x = " + X1;
            }
            else
            {
                double realPart = -B / (2 * A);
                double imPart = Math.Sqrt(-d) / (2 * A);
                Answer = "Уравнение не имеет вещественных корней. Комплексные корни : ";
                if (realPart != 0)
                {
                    Answer += realPart + " ";
                }
                Answer += " +/- ";
                if (imPart != 1)
                    Answer += imPart;
                Answer += "i";
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (A != 0)
            {
                if (Math.Abs(A) != 1)
                    sb.Append(A);
                if (A == -1)
                    sb.Append("-");
                sb.Append("x^2");
            }
            if (B != 0)
            {
                if (sb.Length > 0)
                    sb.Append(B > 0 ? " + " : " - ");
                else if (B < 0)
                    sb.Append("-");
                if (Math.Abs(B) != 1)
                    sb.Append( Math.Abs(B));
                sb.Append("x");
            }
            if (sb.Length > 0)
            {
                if (C != 0)
                {
                    sb.Append(C > 0 ? " + " : " - ");
                    sb.Append(Math.Abs(C));
                }
            }
            else
                sb.Append(C);
            sb.Append(" = 0");
            return sb.ToString();
        }
    }
}
