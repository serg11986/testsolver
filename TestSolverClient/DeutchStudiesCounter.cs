﻿using System;
using System.Collections.Generic;

namespace TestCaseProblemSolver
{
    public static class DeutchStudiesCounter
    {
        public static List<double> PossiblePrimarySchoolStudentsPercents(int allStudentsCount)
        {
            var possiblePercents = new List<double>();

            for (int i = 0; i <= allStudentsCount; i++)
                possiblePercents.Add((double)i / allStudentsCount * 100);

            return possiblePercents;
        }

        public static List<double> PossibleDeutchStudiesPercents(int allStudentsCount, double primarySchoolStudentsCountPercent)
        {
            int primarySchoolStudentsCount = (int)Math.Round((double)allStudentsCount / 100 * primarySchoolStudentsCountPercent);
            int highSchoolStudentsCount = allStudentsCount - primarySchoolStudentsCount;
            var possiblePercents = new List<double>();

            for (int i = 0; i <= highSchoolStudentsCount; i++)
                possiblePercents.Add((double)i / highSchoolStudentsCount * 100);

            return possiblePercents;
        }


        public static int CountDeutchStudies(int allStudentsCount, double primarySchoolStudentsCountPercent, double deutchStudiesPercent)
        {
            double primarySchoolStudentsCount = (double)allStudentsCount / 100 * primarySchoolStudentsCountPercent;

            if (Math.Abs(Math.Round(primarySchoolStudentsCount) - primarySchoolStudentsCount) > 0.00001)
            {
                throw new ArgumentException("При общем количестве учеников, равном " + allStudentsCount +
                    ", процент учеников начальной школы не может быть равен " + primarySchoolStudentsCountPercent +
                    " , т.к. в этом случае число учеников начальной школы не является целым (в этом случае оно равно " +
                    primarySchoolStudentsCount + ")");
            }

            int absPrimarySchoolStudentsCount = (int)Math.Round(primarySchoolStudentsCount);
            int highSchoolStudentsCount = allStudentsCount - absPrimarySchoolStudentsCount;

            double deutchStudiesCount = (double)highSchoolStudentsCount / 100 * deutchStudiesPercent;

            if (Math.Abs(Math.Round(deutchStudiesCount) - deutchStudiesCount) > 0.00001)
            {
                throw new ArgumentException("При общем количестве учеников, равном " + allStudentsCount +
                   ",  среди которых " + primarySchoolStudentsCountPercent +
                   " % учатся в начальной школе (т.е. кол-во учеников начальной школы равно " + absPrimarySchoolStudentsCount +
                   ", а кол-во учеников старшей и средней школы равно " + highSchoolStudentsCount +
                   "), процент изучающих немецкий язык не может быть равен " + deutchStudiesPercent,
                   "т.к " + deutchStudiesPercent + "% от " + highSchoolStudentsCount + " = " + deutchStudiesCount +
                   ", т.е. не является целым числом");
            }

            return (int)Math.Round(deutchStudiesCount);
        }



    }
}
