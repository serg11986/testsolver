﻿using System;
using System.Windows.Forms;
using TestCaseProblemSolver;

namespace TestSolverClient
{
    public partial class Form1 : Form
    {

        bool keyUpHandling; //обрабатывается ли событие отпускания клавиши - это нужно знать для того, 
        //чтобы лишний раз не обрабатывать событие смены значения в NumericUpDown-контролах (т.к. при обработке отпускания
        //клавиши оно почему-то вызывается)

        EqSolver eqSolver = new EqSolver();

        MotorVesselSpeedCounter speedCounter = new MotorVesselSpeedCounter();

        public Form1()
        {
            InitializeComponent();
            nudDeutchX.Value = 10;
            CountFib();
            CountFact();
            DisplayAndSolveEq();
            CountAndDisplayVesselSpeed();
        }        

        #region Count Fib

        private void nudFibNumber_ValueChanged(object sender, EventArgs e)
        {
            if (!keyUpHandling)
                CountFib();
        }

        private void nudFibNumber_KeyUp(object sender, KeyEventArgs e)
        {
            keyUpHandling = true;
            CountFib();
            keyUpHandling = false;
        }

        private void CountFib()
        {
            int number = (int)nudFibNumber.Value;
            try
            {
                long val = FibCounter.FibNumber(number);
                tbFibAnswer.Text = number + "-е число Фибоначчи равно " + val;
            }
            catch
            {
                tbFibAnswer.Text = "Введено слишком большое для текущей точности подсчета число";
            }
        }

        #endregion

        #region Count Fact

        private void nudFactNumber_ValueChanged(object sender, EventArgs e)
        {
            if (!keyUpHandling)
                CountFact();
        }

        private void nudFactNumber_KeyUp(object sender, KeyEventArgs e)
        {
            keyUpHandling = true;
            CountFact();
            keyUpHandling = false;
        }

        private void CountFact()
        {
            int number = (int)nudFactNumber.Value;
            try
            {
                long val = FactCounter.FactOf(number);
                tbFactAnswer.Text = "Факториал от " + number + " равен " + val;
            }
            catch
            {
                tbFactAnswer.Text = "Введено слишком большое для текущей точности подсчета число";
            }
        }

        #endregion

        #region InitRand

        private void btnInitRand_Click(object sender, EventArgs e)
        {
            RandomInitializer.Initialize(out int a, out int b, out int c, out int d);
            lblAVal.Text = a + "";
            lblBVal.Text = b + "";
            lblCVal.Text = c + "";
            lblDVal.Text = d + "";
        }

        #endregion

        #region Solve Equation

        private void nudEq_ValueChanged(object sender, EventArgs e)
        {
            if (!keyUpHandling)
                DisplayAndSolveEq();
        }
        private void nudEq_KeyUp(object sender, KeyEventArgs e)
        {
            keyUpHandling = true;
            DisplayAndSolveEq();
            keyUpHandling = false;
        }

        void DisplayEq()
        {
            tbEqAnsw.Clear();
            lblEq.Text = eqSolver + "";
        }

        private void DisplayAndSolveEq()
        {

            eqSolver.A = (int)nudEqA.Value;
            eqSolver.B = (int)nudEqB.Value;
            eqSolver.C = (int)nudEqC.Value;
            DisplayEq();
            eqSolver.Solve();
            tbEqAnsw.Text = eqSolver.Answer;
        }

        #endregion

        #region Count DeutchStudyingStudents

        private void nudDeutchX_ValueChanged(object sender, EventArgs e)
        {
            tbDeutchAnsw.Clear();
            cbDeutchY.Items.Clear();
            cbDeutchZ.Items.Clear();
            var possiblePercents = DeutchStudiesCounter.PossiblePrimarySchoolStudentsPercents((int)nudDeutchX.Value);
            foreach (double percent in possiblePercents)
                cbDeutchY.Items.Add(percent);
        }

        private void cbDeutchY_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbDeutchAnsw.Clear();
            cbDeutchZ.Items.Clear();
            var possiblePercents
                = DeutchStudiesCounter.PossibleDeutchStudiesPercents((int)nudDeutchX.Value, (double)cbDeutchY.SelectedItem);
            foreach (double percent in possiblePercents)
                cbDeutchZ.Items.Add(percent);
        }


        private void cbDeutchZ_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDeutchY.SelectedItem == null || cbDeutchZ.SelectedItem == null)
                return;

            try
            {
                int res = DeutchStudiesCounter.CountDeutchStudies(
                    (int)nudDeutchX.Value,
                    (double)cbDeutchY.SelectedItem,
                    (double)cbDeutchZ.SelectedItem);
                tbDeutchAnsw.Text = "Ответ: " + res;
            }
            catch (ArgumentException ex)
            {
                tbDeutchAnsw.Text = "Во время подсчета произошла ошибка, скорее всего, введены некорректные данные:\r\n" +
                    ex.Message;
            }
            catch { }

        }

        #endregion

        #region Count Vessel Speed

        private void nudVessel_ValueChanged(object sender, EventArgs e)
        {
            if (!keyUpHandling)
                CountAndDisplayVesselSpeed();
        }

        private void nudVessel_KeyUp(object sender, KeyEventArgs e)
        {
            keyUpHandling = true;
            CountAndDisplayVesselSpeed();
            keyUpHandling = false;
        }

        private void CountAndDisplayVesselSpeed()
        {
            speedCounter.D = (int)nudVesselD.Value;
            speedCounter.X = (int)nudVesselX.Value;
            speedCounter.S = (int)nudVesselS.Value;
            speedCounter.Solve();
            tbVesselAnsw.Text = speedCounter.Answer;
            tbVesselAnsw.Text += "\r\n[Ответ получен в результате решения следующего уравнения: " + speedCounter.Eq + "]";
        }

        #endregion
    }
}
