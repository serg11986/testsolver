﻿namespace TestSolverClient
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.nudFibNumber = new System.Windows.Forms.NumericUpDown();
            this.tbFibAnswer = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.nudFactNumber = new System.Windows.Forms.NumericUpDown();
            this.tbFactAnswer = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnInitRand = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDVal = new System.Windows.Forms.Label();
            this.lblCVal = new System.Windows.Forms.Label();
            this.lblBVal = new System.Windows.Forms.Label();
            this.lblAVal = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.nudEqC = new System.Windows.Forms.NumericUpDown();
            this.nudEqB = new System.Windows.Forms.NumericUpDown();
            this.nudEqA = new System.Windows.Forms.NumericUpDown();
            this.tbEqAnsw = new System.Windows.Forms.TextBox();
            this.lblEq = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tbDeutchAnsw = new System.Windows.Forms.TextBox();
            this.cbDeutchZ = new System.Windows.Forms.ComboBox();
            this.cbDeutchY = new System.Windows.Forms.ComboBox();
            this.nudDeutchX = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tbVesselAnsw = new System.Windows.Forms.TextBox();
            this.nudVesselS = new System.Windows.Forms.NumericUpDown();
            this.nudVesselD = new System.Windows.Forms.NumericUpDown();
            this.nudVesselX = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFibNumber)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFactNumber)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqA)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeutchX)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselX)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Emoji", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 383);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.nudFibNumber);
            this.tabPage1.Controls.Add(this.tbFibAnswer);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(792, 352);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Фибоначчи";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(614, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введите номер числа из последовательности Фибоначчи, которое нужно посчитать:";
            // 
            // nudFibNumber
            // 
            this.nudFibNumber.Location = new System.Drawing.Point(354, 69);
            this.nudFibNumber.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudFibNumber.Name = "nudFibNumber";
            this.nudFibNumber.Size = new System.Drawing.Size(80, 29);
            this.nudFibNumber.TabIndex = 2;
            this.nudFibNumber.ValueChanged += new System.EventHandler(this.nudFibNumber_ValueChanged);
            this.nudFibNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudFibNumber_KeyUp);
            // 
            // tbFibAnswer
            // 
            this.tbFibAnswer.Location = new System.Drawing.Point(6, 123);
            this.tbFibAnswer.Multiline = true;
            this.tbFibAnswer.Name = "tbFibAnswer";
            this.tbFibAnswer.ReadOnly = true;
            this.tbFibAnswer.Size = new System.Drawing.Size(780, 178);
            this.tbFibAnswer.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.nudFactNumber);
            this.tabPage2.Controls.Add(this.tbFactAnswer);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 352);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Факториал";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(398, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Введите число, факториал которого нужно посчитать:";
            // 
            // nudFactNumber
            // 
            this.nudFactNumber.Location = new System.Drawing.Point(348, 66);
            this.nudFactNumber.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudFactNumber.Name = "nudFactNumber";
            this.nudFactNumber.Size = new System.Drawing.Size(88, 29);
            this.nudFactNumber.TabIndex = 6;
            this.nudFactNumber.ValueChanged += new System.EventHandler(this.nudFactNumber_ValueChanged);
            this.nudFactNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudFactNumber_KeyUp);
            // 
            // tbFactAnswer
            // 
            this.tbFactAnswer.Location = new System.Drawing.Point(6, 116);
            this.tbFactAnswer.Multiline = true;
            this.tbFactAnswer.Name = "tbFactAnswer";
            this.tbFactAnswer.ReadOnly = true;
            this.tbFactAnswer.Size = new System.Drawing.Size(780, 179);
            this.tbFactAnswer.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnInitRand);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.lblDVal);
            this.tabPage3.Controls.Add(this.lblCVal);
            this.tabPage3.Controls.Add(this.lblBVal);
            this.tabPage3.Controls.Add(this.lblAVal);
            this.tabPage3.Location = new System.Drawing.Point(4, 27);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(792, 352);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Случайные значения";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnInitRand
            // 
            this.btnInitRand.Location = new System.Drawing.Point(23, 43);
            this.btnInitRand.Name = "btnInitRand";
            this.btnInitRand.Size = new System.Drawing.Size(746, 67);
            this.btnInitRand.TabIndex = 8;
            this.btnInitRand.Text = "Нажмите, чтобы присвоить случайные значения";
            this.btnInitRand.UseVisualStyleBackColor = true;
            this.btnInitRand.Click += new System.EventHandler(this.btnInitRand_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(313, 288);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "d =";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(313, 243);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 18);
            this.label8.TabIndex = 6;
            this.label8.Text = "c =";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(313, 206);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 18);
            this.label9.TabIndex = 5;
            this.label9.Text = "b =";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(313, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 18);
            this.label10.TabIndex = 4;
            this.label10.Text = "a =";
            // 
            // lblDVal
            // 
            this.lblDVal.AutoSize = true;
            this.lblDVal.Location = new System.Drawing.Point(367, 288);
            this.lblDVal.Name = "lblDVal";
            this.lblDVal.Size = new System.Drawing.Size(17, 18);
            this.lblDVal.TabIndex = 3;
            this.lblDVal.Text = "?";
            // 
            // lblCVal
            // 
            this.lblCVal.AutoSize = true;
            this.lblCVal.Location = new System.Drawing.Point(367, 243);
            this.lblCVal.Name = "lblCVal";
            this.lblCVal.Size = new System.Drawing.Size(17, 18);
            this.lblCVal.TabIndex = 2;
            this.lblCVal.Text = "?";
            // 
            // lblBVal
            // 
            this.lblBVal.AutoSize = true;
            this.lblBVal.Location = new System.Drawing.Point(367, 206);
            this.lblBVal.Name = "lblBVal";
            this.lblBVal.Size = new System.Drawing.Size(17, 18);
            this.lblBVal.TabIndex = 1;
            this.lblBVal.Text = "?";
            // 
            // lblAVal
            // 
            this.lblAVal.AutoSize = true;
            this.lblAVal.Location = new System.Drawing.Point(367, 167);
            this.lblAVal.Name = "lblAVal";
            this.lblAVal.Size = new System.Drawing.Size(17, 18);
            this.lblAVal.TabIndex = 0;
            this.lblAVal.Text = "?";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.nudEqC);
            this.tabPage4.Controls.Add(this.nudEqB);
            this.tabPage4.Controls.Add(this.nudEqA);
            this.tabPage4.Controls.Add(this.tbEqAnsw);
            this.tabPage4.Controls.Add(this.lblEq);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Location = new System.Drawing.Point(4, 27);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(792, 352);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Уравнение";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // nudEqC
            // 
            this.nudEqC.Location = new System.Drawing.Point(342, 167);
            this.nudEqC.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudEqC.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.nudEqC.Name = "nudEqC";
            this.nudEqC.Size = new System.Drawing.Size(120, 29);
            this.nudEqC.TabIndex = 9;
            this.nudEqC.ValueChanged += new System.EventHandler(this.nudEq_ValueChanged);
            this.nudEqC.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudEq_KeyUp);
            // 
            // nudEqB
            // 
            this.nudEqB.Location = new System.Drawing.Point(342, 124);
            this.nudEqB.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudEqB.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.nudEqB.Name = "nudEqB";
            this.nudEqB.Size = new System.Drawing.Size(120, 29);
            this.nudEqB.TabIndex = 8;
            this.nudEqB.ValueChanged += new System.EventHandler(this.nudEq_ValueChanged);
            this.nudEqB.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudEq_KeyUp);
            // 
            // nudEqA
            // 
            this.nudEqA.Location = new System.Drawing.Point(342, 81);
            this.nudEqA.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudEqA.Minimum = new decimal(new int[] {
            1000000,
            0,
            0,
            -2147483648});
            this.nudEqA.Name = "nudEqA";
            this.nudEqA.Size = new System.Drawing.Size(120, 29);
            this.nudEqA.TabIndex = 7;
            this.nudEqA.ValueChanged += new System.EventHandler(this.nudEq_ValueChanged);
            this.nudEqA.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudEq_KeyUp);
            // 
            // tbEqAnsw
            // 
            this.tbEqAnsw.Location = new System.Drawing.Point(24, 259);
            this.tbEqAnsw.Multiline = true;
            this.tbEqAnsw.Name = "tbEqAnsw";
            this.tbEqAnsw.ReadOnly = true;
            this.tbEqAnsw.Size = new System.Drawing.Size(740, 85);
            this.tbEqAnsw.TabIndex = 6;
            // 
            // lblEq
            // 
            this.lblEq.AutoSize = true;
            this.lblEq.Location = new System.Drawing.Point(351, 225);
            this.lblEq.Name = "lblEq";
            this.lblEq.Size = new System.Drawing.Size(43, 18);
            this.lblEq.TabIndex = 5;
            this.lblEq.Text = "0 = 0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 18);
            this.label6.TabIndex = 3;
            this.label6.Text = "C =";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(287, 135);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 18);
            this.label5.TabIndex = 2;
            this.label5.Text = "B =";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(287, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "A = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(198, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(403, 18);
            this.label3.TabIndex = 0;
            this.label3.Text = "Введите коэффициенты для уравнения Ax^2+Bx+C=0:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tbDeutchAnsw);
            this.tabPage5.Controls.Add(this.cbDeutchZ);
            this.tabPage5.Controls.Add(this.cbDeutchY);
            this.tabPage5.Controls.Add(this.nudDeutchX);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.textBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 27);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(792, 352);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Немецкий язык";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tbDeutchAnsw
            // 
            this.tbDeutchAnsw.Location = new System.Drawing.Point(18, 274);
            this.tbDeutchAnsw.Multiline = true;
            this.tbDeutchAnsw.Name = "tbDeutchAnsw";
            this.tbDeutchAnsw.ReadOnly = true;
            this.tbDeutchAnsw.Size = new System.Drawing.Size(766, 70);
            this.tbDeutchAnsw.TabIndex = 9;
            // 
            // cbDeutchZ
            // 
            this.cbDeutchZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeutchZ.FormattingEnabled = true;
            this.cbDeutchZ.Location = new System.Drawing.Point(148, 242);
            this.cbDeutchZ.Name = "cbDeutchZ";
            this.cbDeutchZ.Size = new System.Drawing.Size(305, 26);
            this.cbDeutchZ.TabIndex = 8;
            this.cbDeutchZ.SelectedIndexChanged += new System.EventHandler(this.cbDeutchZ_SelectedIndexChanged);
            // 
            // cbDeutchY
            // 
            this.cbDeutchY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeutchY.FormattingEnabled = true;
            this.cbDeutchY.Location = new System.Drawing.Point(148, 190);
            this.cbDeutchY.Name = "cbDeutchY";
            this.cbDeutchY.Size = new System.Drawing.Size(305, 26);
            this.cbDeutchY.TabIndex = 7;
            this.cbDeutchY.SelectedIndexChanged += new System.EventHandler(this.cbDeutchY_SelectedIndexChanged);
            // 
            // nudDeutchX
            // 
            this.nudDeutchX.Location = new System.Drawing.Point(148, 136);
            this.nudDeutchX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDeutchX.Name = "nudDeutchX";
            this.nudDeutchX.Size = new System.Drawing.Size(305, 29);
            this.nudDeutchX.TabIndex = 6;
            this.nudDeutchX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDeutchX.ValueChanged += new System.EventHandler(this.nudDeutchX_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(473, 245);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(22, 18);
            this.label14.TabIndex = 5;
            this.label14.Text = "%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(101, 245);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 18);
            this.label15.TabIndex = 4;
            this.label15.Text = "z =";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(473, 194);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(22, 18);
            this.label13.TabIndex = 3;
            this.label13.Text = "%";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(101, 194);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 18);
            this.label12.TabIndex = 2;
            this.label12.Text = "y =";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(101, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 18);
            this.label11.TabIndex = 1;
            this.label11.Text = "x =";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(18, 30);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(766, 73);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tbVesselAnsw);
            this.tabPage6.Controls.Add(this.nudVesselS);
            this.tabPage6.Controls.Add(this.nudVesselD);
            this.tabPage6.Controls.Add(this.nudVesselX);
            this.tabPage6.Controls.Add(this.label16);
            this.tabPage6.Controls.Add(this.label17);
            this.tabPage6.Controls.Add(this.label18);
            this.tabPage6.Controls.Add(this.textBox2);
            this.tabPage6.Location = new System.Drawing.Point(4, 27);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(792, 352);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Теплоходы";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tbVesselAnsw
            // 
            this.tbVesselAnsw.Location = new System.Drawing.Point(8, 274);
            this.tbVesselAnsw.Multiline = true;
            this.tbVesselAnsw.Name = "tbVesselAnsw";
            this.tbVesselAnsw.ReadOnly = true;
            this.tbVesselAnsw.Size = new System.Drawing.Size(766, 70);
            this.tbVesselAnsw.TabIndex = 16;
            // 
            // nudVesselS
            // 
            this.nudVesselS.Location = new System.Drawing.Point(222, 230);
            this.nudVesselS.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVesselS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselS.Name = "nudVesselS";
            this.nudVesselS.Size = new System.Drawing.Size(120, 29);
            this.nudVesselS.TabIndex = 15;
            this.nudVesselS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselS.ValueChanged += new System.EventHandler(this.nudVessel_ValueChanged);
            this.nudVesselS.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudVessel_KeyUp);
            // 
            // nudVesselD
            // 
            this.nudVesselD.Location = new System.Drawing.Point(222, 187);
            this.nudVesselD.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVesselD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselD.Name = "nudVesselD";
            this.nudVesselD.Size = new System.Drawing.Size(120, 29);
            this.nudVesselD.TabIndex = 14;
            this.nudVesselD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselD.ValueChanged += new System.EventHandler(this.nudVessel_ValueChanged);
            this.nudVesselD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudVessel_KeyUp);
            // 
            // nudVesselX
            // 
            this.nudVesselX.Location = new System.Drawing.Point(222, 144);
            this.nudVesselX.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudVesselX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselX.Name = "nudVesselX";
            this.nudVesselX.Size = new System.Drawing.Size(120, 29);
            this.nudVesselX.TabIndex = 13;
            this.nudVesselX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudVesselX.ValueChanged += new System.EventHandler(this.nudVessel_ValueChanged);
            this.nudVesselX.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudVessel_KeyUp);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(167, 241);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 18);
            this.label16.TabIndex = 12;
            this.label16.Text = "S =";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(167, 198);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 18);
            this.label17.TabIndex = 11;
            this.label17.Text = "D =";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(167, 155);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 18);
            this.label18.TabIndex = 10;
            this.label18.Text = "X = ";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(8, 30);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(766, 100);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 383);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFibNumber)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFactNumber)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudEqA)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDeutchX)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVesselX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudFibNumber;
        private System.Windows.Forms.TextBox tbFibAnswer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudFactNumber;
        private System.Windows.Forms.TextBox tbFactAnswer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnInitRand;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblDVal;
        private System.Windows.Forms.Label lblCVal;
        private System.Windows.Forms.Label lblBVal;
        private System.Windows.Forms.Label lblAVal;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.NumericUpDown nudEqC;
        private System.Windows.Forms.NumericUpDown nudEqB;
        private System.Windows.Forms.NumericUpDown nudEqA;
        private System.Windows.Forms.TextBox tbEqAnsw;
        private System.Windows.Forms.Label lblEq;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudDeutchX;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cbDeutchZ;
        private System.Windows.Forms.ComboBox cbDeutchY;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbDeutchAnsw;
        private System.Windows.Forms.TextBox tbVesselAnsw;
        private System.Windows.Forms.NumericUpDown nudVesselS;
        private System.Windows.Forms.NumericUpDown nudVesselD;
        private System.Windows.Forms.NumericUpDown nudVesselX;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}

