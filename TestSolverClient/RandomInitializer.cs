﻿using System;

namespace TestCaseProblemSolver
{
    public static class RandomInitializer
    {
        public static void Initialize(out int a, out int b, out int c, out int d)
        {
            var r = new Random();

            int minValue = 10, maxValue = 21;

            a = r.Next(minValue, maxValue);

            b = a;
            while (b == a)
                b = r.Next(minValue, maxValue);

            c = b;
            while (c == b || c == a)
                c = r.Next(minValue, maxValue);

            d = c;
            while (d == c || d == b || d == a)
                d = r.Next(minValue, maxValue);
        }
    }
}
